Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Horde Ticket-tracking application
Upstream-Contact: Horde <horde@lists.horde.org>
                  Horde development <dev@lists.horde.org>
Source: http://pear.horde.org/

Files: *
Copyright:
 2001-2002, Robert E. Coyle <robertecoyle@hotmail.com>
 1999-2012, Horde LLC (http://www.horde.org/)
 2001-2017, Horde LLC (http://www.horde.org/)
 2002-2017, Horde LLC (http://www.horde.org/)
 2003-2017, Horde LLC (http://www.horde.org/)
 2004-2017, Horde LLC (http://www.horde.org/)
 2005-2017, Horde LLC (http://www.horde.org/)
 2006-2017, Horde LLC (http://www.horde.org/)
 2007-2017, Horde LLC (http://www.horde.org/)
 2008-2017, Horde LLC (http://www.horde.org/)
 2010-2017, Horde LLC (http://www.horde.org/)
 2011-2017, Horde LLC (http://www.horde.org/)
 2012-2017, Horde LLC (http://www.horde.org/)
 2014, Horde LLC (http://www.horde.org/)
License: BSD-2-clause

Files: whups-*/locale/fi/*
Copyright: 2004-2012, Leena Heino <liinu@uta.fi>
License: BSD-2-clause

Files: whups-*/locale/fr/*
Copyright: 2002, Thierry Thomas <thierry@pompo.net>
License: BSD-2-clause

Files: whups-*/locale/ru/*
Copyright: Illya Belov <belov@iop.irkps.ru>
           2006, Alexey Zakharov <baber@mosga.net>
License: BSD-2-clause

Files: whups-*/locale/cs/*
Copyright: 2002, Pavel Chytil
License: BSD-2-clause

Files: whups-*/locale/zh_CN/*
Copyright: 2002, David Chang
License: BSD-2-clause

Files: whups-*/locale/zh_TW/*
Copyright: 2002, David Chang
License: BSD-2-clause

Files: debian/*
Copyright: 2012-2019, Mathieu Parent <math.parent@gmail.com>
           2020, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: BSD-2-clause

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice, this
 list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
